FROM archlinux:base-devel
ARG USER=user

# User
RUN echo '%wheel ALL=(ALL:ALL) NOPASSWD: ALL' | sudo EDITOR='tee -a' visudo
RUN useradd -m -G wheel -s /bin/bash $USER
USER $USER
WORKDIR /home/$USER

# Proxy
# TODO: support proxy arg

# Basics
RUN sudo gawk -i inplace '!/NoExtract/' /etc/pacman.conf
RUN sudo pacman -Syu --noconfirm
RUN sudo pacman -S pacman --noconfirm
RUN sudo pacman -S man-db man-pages --noconfirm
RUN mandb
RUN sudo pacman -S bash-completion openssh git neovim tmux ripgrep bat fd exa npm unzip hugo --noconfirm

# Dotfiles
RUN git clone https://gitlab.com/dboso/dotfiles
RUN cd dotfiles && ./link-dotfiles.sh

# Bash
RUN sudo pacman -S bash-language-server shellcheck --noconfirm
# Lua
RUN sudo pacman -S lua-language-server --noconfirm
# C
RUN sudo pacman -S cmake clang ctags --noconfirm
# Rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y
RUN source ~/.bash_profile && rustup component add rust-src
RUN mkdir -p ~/.local/bin
RUN source ~/.bash_profile && curl -L https://github.com/rust-analyzer/rust-analyzer/releases/latest/download/rust-analyzer-x86_64-unknown-linux-gnu.gz | gunzip -c - > ~/.local/bin/rust-analyzer
RUN chmod +x ~/.local/bin/rust-analyzer

ENTRYPOINT [ "/bin/bash", "-l" ]
