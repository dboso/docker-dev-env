# Docker developer environment

Simple developer environment

Base image: [archlinux:base-devel](https://hub.docker.com/layers/archlinux/library/archlinux/base-devel/images/sha256-e1be09cb704212345af41fbd6f9039dba109d8313c634a6584b0eeb34fca00f6)

Dotfiles: [dotfiles](https://gitlab.com/dboso/dotfiles/)

Supported languages
* Bash
* Lua
* C
* Rust

## Requirements
`docker`

## Build
Build docker image

`docker build --no-cache --build-arg USER=<USER> -t <IMAGE_NAME> .`

The build argument `USER` denotes the user (and home directory) that will be created inside the container.
If omitted a default user named `user` is created

## Run
Start container

`docker run -it --rm <IMAGE_NAME>`

Start container with mounted directory

`docker run -it --rm -v <HOST_DIR>:<CONTAINER_DIR> <IMAGE_NAME>`

Start container with current directory mounted

`docker run -it --rm -v ${PWD}:<CONTAINER_DIR> <IMAGE_NAME>`

Share network with host

`docker run -it --rm --network host <IMAGE_NAME>`

Pass environment variable to container

`docker run -it --rm -e TERM=alacritty <IMAGE_NAME>`

Set hostname in container

`docker run -it --rm -h docker-dev-env <IMAGE_NAME>`

## Build and run example
`docker build --no-cache --build-arg USER=dboso -t dev-env .`

`docker run -it --rm --network host -e TERM=alacritty -h docker-dev-env -v ~/code:/home/dboso/code -v ~/.ssh:/home/dboso/.ssh dev-env`
